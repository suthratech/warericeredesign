   <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand">
                  <i class="fa fa-bars"></i>
                  </button>
                  <div id="logo-left" class="navbar-brand page-scroll">
                     <a href="index.php"><img src="img/logo.png" class="img-responsive" alt=""></a>
                  </div>
               </div>
               <!-- /.navbar-header -->
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse" id="navbar-brand">
                  <ul class="nav navbar-nav page-scroll navbar-right">
                  <li><a href="index.php#page-top">Home</a></li>
                  <li><a href="index.php#services">Services</a></li>
                  <li><a href="index.php#about">About</a></li>
                  <li><a href="index.php#reviews">Reviews</a></li>
                  <li><a href="index.php#menu">Menu</a></li>
                  <li><a href="index.php#catering">Catering</a></li>
                  <li><a href="index.php#gallery">Gallery</a></li>
                  <li><a href="index.php#team">Team</a></li>
                  <li><a href="index.php#contact">Contact</a></li>
                  <!-- Dropdown -->
                  <li class="dropdown active">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages<b class="caret"></b></a>
                     <ul class="dropdown-menu">
                        <li><a href="blog-home.html">Blog Home</a></li>
                        <li><a href="blog-single.html">Blog Single</a></li>
                        <li class="divider"></li>
                        <li><a href="elements.html">Elements</a></li>
                     </ul>
                  </li>
               </ul>
               </div>
               <!-- /.navbar-collapse -->
            </div>
            <!-- /container -->